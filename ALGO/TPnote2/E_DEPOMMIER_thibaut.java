class E_DEPOMMIER_thibaut extends Program {
    

    // ------------------------------------------------------
    // CONSTANTES ET FONCTIONS PREDEFINIES POUR PARTIE 1 et PARTIE 2
    // Vous n'avez rien a modifier ici
    // ------------------------------------------------------

    // Les operations
    
    /** L'operation AVANCER. */
    final char AVANCER = 'A';
    /** L'operation REPARER. */
    final char REPARER = 'R';
    /** Utilise pour indiquer qu'une operation a deja ete executee. Utile pour la Partie 2. */
    final char FAIT = '-';

    // Les elements de la configuration
    /** Une case ROUTE. */
    final char ROUTE = '#';
    /** Une case TROU. */
    final char TROU = '.';
    /** Une case PION. */
    final char PION = 'P';

    /** Representation d'une configuration sous forme de chaine de caracteres.
     * a utiliser pour les affichages.
     */
    String configurationToString (char[] configuration) {
        String s = "";
            for (int i = 0; i < length(configuration); i++) {
                s = s + configuration[i] + " ";
            }
        return s;
    }


    // ------------------------------------------------------
    // CONSTANTES PREDEFINIES POUR PARTIE 3
    // ------------------------------------------------------
    /** L'operation BAS. */
    final char BAS = 'B';
    /** L'operation DROITE. */
    final char DROITE = 'D';


    // ------------------------------------------------------
    // Question 1. Creer un parcours simple pour tester.
    // 
    // Les deux fonctions qui suivent seront utiles pour tester.
    // Elles permettent de creer deux parcours, l'un avec le pion
    // devant une route, et l'autre avec le pion devant un trou
    //
    // ecrire les fonctions creerConfigurationDeTest1 et creerConfigurationDeTest2
    // pour qu'elles passent le test testCreerParcoursDeTest
    // ------------------------------------------------------

    void testCreerConfigurationDeTest1 () {
        assertArrayEquals(new char[]{PION, ROUTE, ROUTE, TROU, ROUTE},
                          creerConfigurationDeTest1());
    }
    
    /** Retourne cette configuration de longueur cinq
     *  [PION, ROUTE, ROUTE, TROU, ROUTE]
     *  (cad: P # # . # )
     */
    char[] creerConfigurationDeTest1() {
        return new char[]{PION, ROUTE, ROUTE, TROU, ROUTE};
    }

    void testCreerConfigurationDeTest2 () {
        assertArrayEquals(new char[]{ROUTE, ROUTE, PION, TROU, ROUTE},
                          creerConfigurationDeTest2());
    }
    
    /** Retourne cette configuration de longueur cinq
     *  [ROUTE, ROUTE, PION, TROU, ROUTE]
     *  (cad: # # P . # )
     */
    char[] creerConfigurationDeTest2() {
        return new char[]{ROUTE, ROUTE, PION, TROU, ROUTE};
    }

    
    // ------------------------------------------------------
    // Question 2: Determiner la position du pion
    //
    // ecrire la fonction positionPion qui determine la position
    // du pion dans une configuration, et qui passe testPositionPion
    //
    // On suppose que la configuration donnee en paramètre 
    // contient forcement un pion.
    // ------------------------------------------------------    

    void testPositionPion () {
        char[] configuration1 = creerConfigurationDeTest1();
        assertEquals(0, positionPion(configuration1));
        
        char[] configuration2 = creerConfigurationDeTest2();
        assertEquals(2, positionPion(configuration2));
    }
    
    /** Retourne l'indice du pion.
     * @param configuration : une configuration
     * @return l'indice du pion dans cette configuration
     */
    int positionPion (char[] configuration) {
        int cpt=0;
        boolean trouve = false;
        int res = 42; 
        while(!trouve){
            if (configuration[cpt]=='P') {
                res = cpt;
                trouve = true;
            }
            cpt++;
        }

        return res;
    }
    
    // ------------------------------------------------------
    // Question 3: Faire avancer le pion
    // 
    // Vous devez ecrire la fonction avancer qui implemente
    // l'instruction AVANCER, et qui passe testAvancer
    // ------------------------------------------------------    

    void testAvancer () {
        char[] configuration = creerConfigurationDeTest1();
        boolean peutAvancer;

        peutAvancer = avancer(configuration);
        
        assertTrue(peutAvancer);
        assertArrayEquals(new char[]{ROUTE, PION, ROUTE, TROU, ROUTE},configuration);
        
        peutAvancer = avancer(configuration);
        
        assertTrue(peutAvancer);
        assertArrayEquals(new char[]{ROUTE, ROUTE, PION, TROU, ROUTE},configuration);
        
        peutAvancer = avancer(configuration);assertFalse(peutAvancer);
        assertArrayEquals(new char[]{ROUTE, ROUTE, PION, TROU, ROUTE},configuration);
    }
    
    /** Execute l'instruction AVANCER si possible.
     * @param configuration : une configuration
     * @return true si l'operation AVANCER est possible dans cette 
     *         configuration, faux sinon. De plus, si l'operation AVANCER
     *         est possible, alors la configuration est modifiee en consequence.
     */
    boolean avancer (char[] configuration) {
        int positionPion = positionPion(configuration);
        if (configuration[positionPion+1]=='#') {
            configuration[positionPion+1]='P';
            configuration[positionPion]='#';
            return true;

        }
        return false;
    }
    
    // ------------------------------------------------------
    // Question 4: Tester reparer
    //
    // Vous devez ecrire une fonction de test pour la fonction reparer
    // ------------------------------------------------------
    void testReparer () {
        char[] configuration = creerConfigurationDeTest1();
        boolean peutReparer;

        peutReparer = reparer(configuration);
        assertFalse(peutReparer);
        // A COMPLETER : ecrire le code qui permet de tester que l'operation
        // REPARER n'est pas possible pour la configuration donnee par
        // creerConfigurationDeTest1()
        configuration = creerConfigurationDeTest2();
        peutReparer = reparer(configuration);
        assertTrue(peutReparer);
        assertArrayEquals(new char[]{ROUTE, ROUTE, PION, ROUTE, ROUTE},configuration);        
        // A COMPLETER : ecrire le code qui permet de tester que l'operation
        // REPARER est possible pour la configuration donnee par
        // creerConfigurationDeTest2() et la nouvelle configuration obtenue
        // est ROUTE ROUTE PION ROUTE ROUTE
    }

    // ------------------------------------------------------
    // Question 5: Reparer
    //
    // Vous devez ecrire la fonction reparer qui implemente
    // l'instruction REPARER.
    // ------------------------------------------------------   

    /** Execute l'instruction REPARER
     * @param configuration : une configuration
     * @return true si l'operation REPARER est possible dans cette
     *         configuration, faux sinon. De plus, si l'operation REPARER 
    *          est possible, alors la configuration est modifiee en consequence.
     */
    boolean reparer (char[] configuration) {
        int positionPion = positionPion(configuration);
        if (configuration[positionPion+1]=='.') {
            configuration[positionPion+1]='#';
            return true;
        }

        return false;
    }

    // ------------------------------------------------------
    // Question 5: Algorithme principal
    // 
    // Voici la fonction void algorithm().
    // Vous allez ecrire trois fonctions auxiliaires qui correspondent 
    // au trois parties du sujet.
    // 
    // Renommez _algorithm en algorithm, pour qu'elle soit executee.
    // Decommentez la ligne correspondante dans void algorithm() pour tester.
    // Pour l'instant, commencez par decommenter la ligne pour
    // algorithmePrincipalPartie1()
    // ------------------------------------------------------

    void algorithm () {
        // Decommentez une de ces lignes pour tester la Partie 1,
        // la Partie 2 ou la Partie 3

         algorithmePrincipalPartie1();
         algorithmePrincipalPartie2();
         //algorithmePrincipalPartie3();
    }

    // ------------------------------------------------------
    // Question 6: ecrire La fonction algorithmePrincipalPartie1
    // 
    // Pour l'instant cette fonction contient juste deux instructions
    // pour illustrer l'affichage d'une configuration.
    // Vous devez enlever ces deux instructions.
    // La fonction configurationToString vous est fournie, vous ne devez
    // pas l'ecrire.
    //
    // Q 6.1 ecrire la fonction algorithmePrincipalPartie1 en utilisant
    //       comme configuration de depart celle donnee par
    //       creerConfigurationDeTest1 comme configuration
    // Q 6.2 Modifier la fonction pour qu'elle utilise une configuration
    //       aleatoire (voir ci-dessous)
    // ------------------------------------------------------ 


    void algorithmePrincipalPartie1 () {
        // A ENLEVER A PARTIR D'ICI
        // Voici comment on utilise la fonction d'affichage d'une configuration
        //char[] configuration = creerConfigurationDeTest1();
        //println(configurationToString(configuration));
        // A ENLEVER JUSQUE ICI

        // A COMPLETER LA SUITE


        // Creer la configuration initiale
        // VERSION 1 : Utiliser la configuration de test donnee par la
        // fonction creerConfigurationDeTest1
        // VERSION 2 : (voir ci-dessous) : Generer un parcours aleatoire
        // de taille 5 avec 2 trous.
        char[] configuration = creerConfigurationDeTest1();
        boolean saisivalide = false;
        while(!saisivalide || positionPion(configuration)<length(configuration)-1){
            println(configurationToString(configuration));
            println("Que faire ? A=avancer, B=reparer");
            char proposition = readChar();
            if (proposition == 'A') {
                saisivalide = avancer(configuration);
            }
            else if (proposition == 'B') {
                saisivalide = reparer(configuration);
            }
            else{print("Saisi impossible");}
        }
        // Afficher le parcours et demander a l'utilisateur de
        // saisir une action parmi AVANCER ou REPARER
        // Repeter l'affichage et la saisie jusque ce que
        // ou bien la fin du parcours est atteinte
        // ou bien l'action saisie n'est pas possible
	
    }


    // ------------------------------------------------------
    // Question 6.2 : Creer une configuration aleatoire
    // 
    // Creer une fonction nommee: creerConfigurationAleatoire
    // qui prend en paramètre deux entiers: la taille de la configuration,
    // et le nombre de trous et qui retourne une configuration.
    // Le pion doit se trouver a la première case de cette configuration.
    // On suppose que le nombre de trous demande est inferieur a la taille
    // demandee.
    // 
    // Vous devez d'abord donner la signature de cette fonction, 
    // puis ecrire son corps, et finalement l'utiliser dans
    // algorithmePrincipalPartie1 pour creer la configuration initiale
    // ------------------------------------------------------

    // A COMPLETER ICI avec le code de la fonction creerConfigurationAleatoire
   /* char[] creerConfigurationAleatoire(int longueur,int nbrDeTrou){
        char[]tab = new char[longueur];
        tab[0]='P';
        for (int cpt=0;cpt < nbrDeTrou ;cpt++ ) {
            
        }
        for (int cpt = 1;cpt < longueur ;cpt++ ) {
            
        }
    }*/



    // ------------------------------------------------------
    // Question 7: Algorithme principal et fonctions supplementaires pour Partie 2
    // 
    // ecrire la fonction algorithmePrincipalPartie2 qui correspond au
    // jeu de la Partie 2.
    // Vous pouvez decommenter la ligne correspondante dans void algorithm()
    // pour l'executer.
    //
    // Il serait judicieux de copier le corps de la fonction algorithmePrincipalPartie1
    // et de le modifier.
    // 
    // Pour le faire, vous devrez repondre a ces questions:
    // - comment representer le programme (c.a.d. la suite d'instructions) ?
    // - qu'est-ce qui change dans le critère d'arret dans la boucle ?
    
    // Il serait une bonne idee d'introduire ces fonctions supplementaires:
    // - une fonction pour saisir le programme
    // - une fonction pour afficher l'etat du programme
    // ------------------------------------------------------ 

    void algorithmePrincipalPartie2() {
        

        char[] configuration = creerConfigurationDeTest1();
        boolean saisivalide = false;
        while(!saisivalide && positionPion(configuration)!=length(configuration)){
            println(configurationToString(configuration));
            String proposition = readString();
            if (length(proposition)==length(configuration)) {
                affichage(configuration,proposition);
                saisivalide = true;
            }
            else{println("Saisi impossible");}
        }
        if (configurationToString(configuration) == configurationGagnant(length(configuration))) {
            println("Tu as gagné !");
        }
        else{println("Tu as perdu");}

    }
    void affichage(char[]configuration,String proposition){
        for (int cpt = 0;cpt<length(proposition) ;cpt++ ) {
            char action = charAt(proposition,cpt);
            println(configurationToString(configuration));
            if (action == 'A') {
                avancer(configuration);
            }
            else if (action == 'B') {
                reparer(configuration);
            }
        }
    }
    String configurationGagnant(int longueur){
        char[]res = new char[longueur];
        for (int cpt = 0;cpt < longueur-1 ;cpt++ ) {
            res[cpt]='#';
        }
        res[longueur-1]='P';
        return configurationToString(res);
    }

    // A COMPLETER ICI AVEC LES FONCTIONS INTRODUITES POUR LA PARTIE 2


    // ------------------------------------------------------
    // Question 8: Algorithme principal et fonctions supplementaires pour Partie 3
    // 
    // ecrire la fonction algorithmePrincipalPartie3 qui correspond au
    // jeu de la Partie 3
    // Il n'est pas demande de pouvoir executer cette fonction.
    // Par contre, elle doit introduire des fonctions supplementaires.
    // et doit pouvoir etre compilee sans erreur.
    // ------------------------------------------------------ 
    void algorithmePrincipalPartie3() {
        // A COMPLETER pour qu'on puisse compiler cette fonction
	
	
    }

    // A COMPLETER avec les fonctions introduites dans algorithmePrincipalPartie3
    // Seules les signatures de ces fonctions sont demandees
    // Pour qu'on puisse compiler, les fonctions non void devront retourner un resultat
    // Choisissez n'importe quel resultat du bon type (par exemple 0 pour int,
    // "" pour String, new char[]{} pour un tableau de char, false pour boolean, etc.
    
}
