class MajMin extends Program {
  void testMinToMaj() {
    assertEquals('A', minToMaj('a'));
    assertEquals('F', minToMaj('f'));
  }
    char minToMaj(char c) {
	
	c -= 32;
	return c;
  }
  void testMajToMin() {
    assertEquals('a', majToMin('A'));
    assertEquals('p', majToMin('P'));
  }
    char majToMin(char c) {

    return (char)(c + 32);
  }
  void testLettreAuHasard() {
    char lettre = lettreAuHasard(); println(lettre);
    assertGreaterThanOrEqual('a', lettre);
    assertLessThanOrEqual('z', lettre);
    // ou: assertTrue('a' <= lettre && lettre <= 'z');
  }
  char lettreAuHasard() {
      double rand = random();
      int codeASCII = (int) (rand * 26) + 97;
      return (char) codeASCII ;
  }
}


