
class tab2d extends Program{
	void algorithm(){
		
		int hauteur = readInt();
		int longueur = readInt();

		affichertab2d(creertab2d(longueur,hauteur));
	}

	void affichertab2d(int[][]tab){
		for (int l = 0;l < length(tab,2);l++ ) {
			for (int h = 0;h < length(tab,1);h++ ) {
				print(tab[h][l]+"\t");
			}
			print("\n");
		}
	}

	int[][] creertab2d(int hauteur, int longueur){
		int cpt = 0;
		int[][]tab = new int[hauteur][longueur];
		for (int l = 0;l < longueur;l++ ) {
			for (int h = 0;h < hauteur;h++ ) {
					cpt++;
					tab[h][l]= cpt;
			}
			
		}
		return tab;
	}
}