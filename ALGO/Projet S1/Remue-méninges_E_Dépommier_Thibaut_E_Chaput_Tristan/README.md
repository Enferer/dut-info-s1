# <NomProjet>

<Nom Prénom> <Nom Prénom>

## Présentation de <NomProjet>

<Mettre ici quelques paragraphes pour présenter votre logiciel:
- fonctionnalités proposées,
- intérêt pédagogique pour les élèves.>

## Utilisation de <NomProjet>

Afin d'utiliser le projet, il doit être suffisant de taper les 
commandes suivantes:
./compile.sh            // lancer la compilation des fichiers
                        // présents dans 'src' et création des 
                        // fichiers '.class' dans 'classes'

Comme certains binômes ont un projet en mode texte et un autre 
en mode graphique, merci de préciser le nom des programmes à 
passer en paramètre au script 'run.sh'

./run.sh <NomDuProgrammeTexte>     (mode texte)
./run.sh <NomDuProgrammeGraphique> (mode graphique si présent)

Pour tous les projets: pensez à mettre l'ap.jar dans le répertoire 'lib' !

Pour les projets en mode graphiques, placez les images dans le répertoire
'ressources' sachant que son contenu est copié dans 'classes' lorsque 
vous faites un 'run'.

