import extensions.CSVFile;
class Projet extends Program{

	Joueur joueur = new Joueur();
	
	void algorithm(){
	
		clearScreen();
		cursor(1,1);
		CSVFile classementJoueur = loadCSV("classementJoueur.csv",';');
	
		boolean jeufini = false;
	
		println("Comment t'appelles tu ?");
		boolean nomCorrect = false;
		do{
			joueur.nom = readString();
			if (!equals(joueur.nom,"")) {
				nomCorrect =true;
			}
		}while(!nomCorrect);

		//String[] cases = {"J-D"," P "," Q "," M "," O "," R "," B "," P "," Q "," M "," O "," R "," B "," P "," Q "," M "," O "," R "," B "," A "};
		String[] cases = plateauAleatoire();    // Génération du plateau
		while(!jeufini){
			clearScreen();
			cursor(1,1);
			afficherPlateau(cases,"Appuie sur entre pour lancer le de\n",joueur);
			readString();
			int deAleatoire = lancerDuDe(cases); 
			cases = deplacementAleatoire(cases,deAleatoire);							// Simulation du lancement de dé et déplacement sur le plateau
			clearScreen();
			cursor(1,1);
			String phrase = "";

			// **********  Détection et lancement du jeu  ***********

			if (charAt(cases[indiceJoueur(cases)],2)=='P') {
				phrase = "Tu es arrive sur une case pendu\nAppuie sur entre pour continuer\n";	
				phrase += "Tu as "+ joueur.points +" point(s)\n";
				afficherPlateau(cases,phrase,joueur);
				readString();
				pendu();
			}
			else if (charAt(cases[indiceJoueur(cases)],2)=='Q') {
				phrase = "Tu es arrive sur une case quizz\nAppuie sur entre pour continuer\n";
				phrase += "Tu as "+ joueur.points +" point(s)\n";
				afficherPlateau(cases,phrase,joueur);
				readString();
				quizz();
			}
			else if (charAt(cases[indiceJoueur(cases)],2)=='O') {
				phrase = "Tu es arrive sur une case operation a trou\nAppuie sur entre pour continuer\n";
				phrase += "Tu as "+ joueur.points +" point(s)\n";
				afficherPlateau(cases,phrase,joueur);
				readString();
				operation();
			}
			else if (charAt(cases[indiceJoueur(cases)],2)=='H') {
				phrase = "Tu es arrive sur une case re-fait le mot\nAppuie sur entre pour continuer\n";
				phrase += "Tu as "+ joueur.points +" point(s)\n";
				afficherPlateau(cases,phrase,joueur);
				readString();
				harry();
			}
			else if (charAt(cases[indiceJoueur(cases)],2)=='B') {
				phrase = "Tu es arrive sur une case Bonus !\nTu gagnes x points !\nAppuie sur entre pour continuer\n";
				phrase += "Tu as "+ joueur.points +" point(s)\n";
				afficherPlateau(cases,phrase,joueur);
				readString();
				joueur.points += 50;
			}
			else if (charAt(cases[indiceJoueur(cases)],2)=='M') {
				phrase = "Tu es arrive sur une case malus\nTu perds x points\nAppuie sur entre pour continuer\n";
				phrase += "Tu as "+ joueur.points +" point(s)\n";
				afficherPlateau(cases,phrase,joueur);
				readString();
				joueur.points -= 50;
			}
			else if (charAt(cases[indiceJoueur(cases)],2)=='A') {
				phrase = "Tu as fini patati patata\n";
				phrase += "Tu as "+ joueur.points +" point(s)\n";
				afficherPlateau(cases,phrase,joueur);
				readString();
				jeufini = true;
			}
			// **********  Fin détection et lancement du jeu  ***********
		}

		clearScreen();
		cursor(1,1);

		ajouterInfoCSV(classementJoueur,joueur,"classementJoueur.csv");
		CSVFile classementJoueurMisAJour = loadCSV("classementJoueur.csv",';');
		String classement = generationClassement(classementJoueurMisAJour);
		print(affichage(classement));
		println("Merci d'avoir joué");

	}

	void harry(){
		
		CSVFile motHarry = loadCSV("mots_harry.csv",';');
		clearScreen();
		cursor(1,1);
		String mot;
		int nbr_De_Vie_Harry = 2;
		print(affichage("Bienvenue sur le jeu \" re-fait le mot \"\n(Explication des regles)\n Appuie sur entre pour commencer\n"));
		readString();
		boolean motTrouve = false;

		boolean jeufini = false;
		//for (int cpt=5;cpt!=0;cpt--) {
		int nbrMotTrouve = 0;
		while(nbrMotTrouve !=5 && nbr_De_Vie_Harry > 0){

			int idxMotLigne = (int)(random()*rowCount(motHarry));
			int idxMotColone = (int)(random()*columnCount(motHarry,idxMotLigne));
			mot = getCell(motHarry,idxMotLigne,idxMotColone);
			println("Il te reste "+(5-nbrMotTrouve)+" mot(s) à trouver");
			println("Tu as encore "+nbr_De_Vie_Harry+" vie");
			delay(100);
			println("Mot à trouver : "+tronquer(mot));
			
			while(!motTrouve && nbr_De_Vie_Harry > 0){
				String reponseU = readString();
				reponseU = toLowerCase(reponseU);
				if(equals(reponseU,mot)){
					println("Le mot était : "+mot+", Bien joué !");
					nbrMotTrouve++;
					motTrouve = true;
				}
				else{
					nbr_De_Vie_Harry--;
					println("Tu n'as pas trouvé le mot tu perds une vie\nIl te reste "+nbr_De_Vie_Harry+" vie(s)");					
				}
			}
			motTrouve = false;

		}
		if (nbr_De_Vie_Harry > 0) {
			joueur.points += 250;
			print(affichage("Bien joue\nTu gagnes 250 points\nAppuie sur entre pour continuer :D\n"));
		}
		else{
			print(affichage("Tu n'as plus de vie\nTu as perdu\nAppuie sur entre pour continuer :D\n"));
		}
		readString();
	}

	String tronquer(String mot){

		String res = "";
		int alea = (int)(random()*2);
		if (length(mot)== 8) {

				int couper = (int)(length(mot)/3);

				String mot_part1 = substring(mot,0,couper+1);
				String mot_part2 = substring(mot,couper+1,length(mot)-couper);
				String mot_part3 = substring(mot,couper+couper+2,length(mot));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
		}
		else if (length(mot)==7) {

				int couper = (int)(length(mot)/3);

				String mot_part1 = substring(mot,0,couper+1);
				String mot_part2 = substring(mot,couper+1,length(mot)-couper);
				String mot_part3 = substring(mot,couper+couper+1,length(mot));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
		}
		else if (length(mot)==6) {

				int couper = (int)(length(mot)/3);

				String mot_part1 = substring(mot,0,couper);
				String mot_part2 = substring(mot,couper,length(mot)-2);
				String mot_part3 = substring(mot,couper+couper,length(mot));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
		}
		else if (length(mot)==5) {

				int couper = (int)(length(mot)/3);

				String mot_part1 = substring(mot,0,couper+1);
				String mot_part2 = substring(mot,couper+1,length(mot)-couper);
				String mot_part3 = substring(mot,length(mot)-couper,length(mot));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
		}
		return res;
	}

	// ******** Fin re-fait le mot ******

	// ******** Début money dorp *******

	void quizz(){
		
		clearScreen();
		cursor(1,1);

		CSVFile question = loadCSV("question_quizz.csv",';');
		
		print(affichage("Quizz/Money-drop\nBienvenue sur le quizz sur l'histoire\n( Definission des regles )\nAppuie sur entree pour continuer\n"));
		readString();
		int points_restant = 500;
		int nombre_de_question = 0;
		boolean jeuPerdu = false;

		while( (nombre_de_question <= 3) && !jeuPerdu){         // Le jeu se fini quand le jeu est perdu ou que le joueur a répond a 4 question
			
			int idxQuestion = (int) (random()*rowCount(question));   // On tire un id de question aléatoirement par rapport au nombre de question disponible dans le csv
			nombre_de_question++;
		
			println("Question "+nombre_de_question+" :");
			println(getCell(question, idxQuestion,0));
			println("1. "+getCell(question,idxQuestion,1));
			println("2. "+getCell(question,idxQuestion,2));
			println("3. "+getCell(question,idxQuestion,3));
			println("4. "+getCell(question,idxQuestion,4));		

			int[] mises = siasieMiseDuJoueur(points_restant);   // On stock dans un tableau les mises du joueur.
			
			int bonne_reponse = stringToInt(getCell(question,idxQuestion,5));   // On va chercher la bonne réponse

			if (mises[bonne_reponse-1]==0) {			// Si le joueur a mise 0 sur la bonne réponse il a perdu
				println("Tu as perdu");
				println("La bonne réponse , était la réponse "+bonne_reponse);
				println("Appuie sur enré pour continuer");
				readString();
				jeuPerdu = true;
			}
			else{
				println("La bonne réponse , était la réponse "+bonne_reponse);
				println("Il te reste "+mises[bonne_reponse-1]+" points");
				points_restant = mises[bonne_reponse-1];
				delay(1000);
			}

		}
		if (!jeuPerdu) {													// Si le jeu n'est pas perdu on lui ajoute les points qu'il lui reste
			println("Bien joué tu gagne : "+points_restant+" points !");
			println("Appuie sur entrée pour continuer.");
			joueur.points += points_restant;
			readString();
		}
		

	}

	
	int[] siasieMiseDuJoueur(int points_restant){
		boolean compte_exact = false;

		int[] mise = new int[4];

		while(!compte_exact){
			int points_restant_temp = points_restant;
			println("Combien mises tu sur la réponse 1 ? (Il te reste "+ points_restant_temp +" à miser )");
			mise[0] = readInt2();
			points_restant_temp -= mise[0];
			
			println("Combien mises tu sur la réponse 2 ? (Il te reste "+ points_restant_temp +" à miser )");
			mise[1] = readInt2();
			points_restant_temp -= mise[1];

			println("Combien mises tu sur la réponse 3 ? (Il te reste "+ points_restant_temp +" à miser )");
			mise[2] = readInt2();
			points_restant_temp -= mise[2];

			println("Combien mises tu sur la réponse 4 ? (Il te reste "+ points_restant_temp +" à miser )");
			mise[3] = readInt2();
			points_restant_temp -= mise[3];

			if ( mise[0]+mise[1]+mise[2]+mise[3] < points_restant) {		// On test si il a miser assez de point ou trop de points
				println("Tu n'as pas misé tous tes points");
			}
			else if ( (mise[0]+mise[1]+mise[2]+mise[3]) > points_restant) {
				println("Tu n'as pas assez de points pour miser autant");
			}
			else{
				compte_exact = true;
			}
		}
		return mise;
	}

	/*void testStringToNum(){
		assertEquals(0, StringtoNum("0"));
		assertEquals(9, StringtoNum("9"));
		assertEquals(12, StringtoNum("12"));
		assertEquals(134, StringtoNum("134"));
	}

	int StringtoNum(String mot){
		int res = 0;													// Remplacer pas String stringToInt( String s ) déja présent dans Program.
		int cpt2 = 0;
		for (int cpt = length(mot);cpt > 0  ;cpt-- ) {
			res += (((int) charAt(mot,cpt-1))-48)*pow(10,cpt2);
			cpt2++;
		}
		return res;
	}*/

	// ******* Fin money drop ********

	//******* Début operation à trou *******

	void operation(){
		clearScreen();
		cursor(1,1);

		print(affichage("Operation a trou\nDefinition des regles\nAppuie sur entree pour continuer\n"));
		
		int niveau_Operation = saisieNiveau();						// on lui demande le niveau de difficulté qu'il veut avoir.
		boolean victoireOperation = false;
		int nbr_de_bonne_reponse=0;
		int nbr_de_vie_operation=2;

		while( !victoireOperation && nbr_de_vie_operation !=0){			// On lui donne des operation a trou jusqu'a temps qu'il est gagné ou qu'il n'est plus de vie
			println("Tu as "+nbr_de_vie_operation+" vie(s).");
			Operation operation = new Operation();						
			operation = generationOperation(niveau_Operation);			// On génére une operation aléatoire
			print(operation.operande1);
			print(operation.operateur);
			println(" ? = " + operation.resultat);
			int proposition = readInt2();
			if (proposition == operation.operande2) {
				nbr_de_bonne_reponse++;
				println("Bonne réponse !");
				delay(2000);
			}
			else{
				nbr_de_vie_operation--;
				println("Mauvaise réponse , tu perds une vie\nIl te reste "+nbr_de_vie_operation+" vie(s)");
			}
			if (nbr_de_bonne_reponse == 5) {
				victoireOperation = true;
			}
		}
		if (victoireOperation) {
			print(affichage("Bien joue tu as gagne !\nTu gagnes : "+niveau_Operation*4 +" points !\nAppuie sur entree pour continuer\n"));
			joueur.points += (niveau_Operation*4);
			readString();
		}
		else{
			print(affichage("Tu as perdu tu n'as plus de vie.\n Tu ne gagnes aucun point\nAppuie sur entree pour continuer\n"));
			readString();
		}

		
	}

	int saisieNiveau(){
		clearScreen();
		cursor(1,1);
		print(affichage("Quelle niveau choisis tu ?\n1. Niveau 1 ( 80 points )\n2. Niveau 2 ( 120 points )\n3. Niveau 3 ( 200 points )\n"));

		boolean saisieCorrect = false;
		while(!saisieCorrect){
			int choix_Niveau_Operation = readInt2();
			if (choix_Niveau_Operation == 1) {
				return 20;
			}
			else if (choix_Niveau_Operation == 2) {
				return 30;
			}
			else if (choix_Niveau_Operation == 3) {
				return 50;
			}
			else{
				println("Saisie incorrect");
			}
		}
		return 0;
	}



	Operation generationOperation(int lvl){
		Operation operation = new Operation();
		operation.operateur = operateurAleatoire();
		operation.operande1 = normbreAleatoire(lvl,operation.operateur);
		operation.operande2 = normbreAleatoire(lvl,operation.operateur);

		if (operation(operation.operande1,operation.operande2,operation.operateur)<0) {  // test pour ne pas avoir de résultat négatif
			int temporaire = operation.operande1;
			operation.operande1 = operation.operande2;
			operation.operande2 = temporaire;
		}

		operation.resultat = operation(operation.operande1,operation.operande2,operation.operateur);

		return operation;
	}

	char operateurAleatoire(){
		int operateur = (int) (random()*3);					// Génération aléatoire de l'operateur
		switch(operateur){
			case 0:
			return '+';
			case 1:
			return '-';
			case 2:
			return '*';
			default:
			return ' ';
		}
	}

	void testOperation(){
		assertEquals(5, operation(3,2,'+'));
		assertEquals(1, operation(3,2,'-'));
		assertEquals(-3, operation(2,5,'-'));
		assertEquals(6, operation(3,2,'*'));
	}


	int operation(int nbr1, int nbr2, char c){			// Réalisatation de l'operation pour avoir le résultat.
		if (c == '+') {
			return nbr1 + nbr2;
		}
		else if (c == '-') {
			return nbr1 - nbr2;
		}
		else if (c == '*') {
			return nbr1 * nbr2;
		}
		else{
			return 0;
		}
	}

	int normbreAleatoire(int lvl,char c){				// On génére un nombre aléatoire grace au lvl et si c'est une multiplication on génére des nombres moins haut.
		if (c == '*') {
			return ((int) (random()*lvl/4))+1;
		}
		else{
			return ((int) (random()*lvl))+1;
		}
	}
	
	// ******** Fin operation a trou *******
	
	// ******** Début du pendu ********


	void pendu(){
		clearScreen();
		cursor(1,1);
		print(affichage("Bienvenue sur le pendu en anglais !\nTu as 10 tentative pour trouver le mot\nQuand il te reste 1 essaie on te donne un indice\n"));
		CSVFile motPendu = loadCSV("mot_pendu.csv",';');
		int idMot =(int)(random()*rowCount(motPendu));
		String mot = getCell(motPendu,idMot,0);
		//println(mot);
		boolean fini = false;
		Mot m1 = newMot(mot);
		int tentative = 10;

		while(!fini){
			if (tentative == 1) {
				println("Indice : Le mot signifie "+getCell(motPendu,idMot,1)+" en français");
			}
			println(toStringCache(m1)+"      | Il te reste "+tentative+" essaies(s)");

			boolean trouve = false;
			char proposition = readChar2();
			for (int cpt = 0;cpt < length(mot) ;cpt++) {
				if (m1.mot[cpt].lettre==proposition) {
					m1.mot[cpt].decouverte = true;
					trouve = true;
				}
			}

			if (!trouve) {
				println("La lettre n'est pas présent dans le mot.");
				tentative--;
			}

			else{
				println("Bien joué la lettre est présent dans le mot !");
			}

			if (equals(toStringCache(m1),toString(m1))) {

				print(affichage("Bien joue tu as trouve le mot\nAppuie sur entre pour continuer\n"));
				joueur.points += 100;
				fini = true;
				readString();
			}

			else if(tentative <= 0){
				println("Tu as perdu");
				fini = true;
			}

		}
	}

	char readChar2(){
		String mot = "  ";
		do{
			mot = readString();
		}while(length(mot)!=1);

		return charAt(mot,0);
	}	

	Lettre newLettre(char l){
		Lettre res = new Lettre();
		res.lettre = l;
		return res;
	}

	String toString(Mot m1){
		String res = "";
		for (int cpt = 0;cpt < length(m1.mot) ; cpt++ ) {
			res += m1.mot[cpt].lettre;
		}
		return res;
	}

	String toStringCache(Mot m1){
		String res = "";
		for (int cpt = 0;cpt < length(m1.mot) ; cpt++ ) {
			if (m1.mot[cpt].decouverte) {
				res += m1.mot[cpt].lettre;
			}
			else{res+='*';}
			}
		return res;
	}

	Mot newMot(String s) {
		Mot m1 = new Mot();
		m1.mot = new Lettre[length(s)];
		
		for(int cpt = 0; cpt < length(s); cpt++){
			m1.mot[cpt]=newLettre(charAt(s,cpt));
		}
		return m1;	
	}
	// ********* Fin pendu  **********

	// ********* ReadInt2() **********
	int readInt2(){
		boolean bonneSaisie=false;
		String mot = readString();
		while(!bonneSaisie){
			bonneSaisie = true;
			for (int cpt=0;cpt < length(mot);cpt++ ) {
				if (!(charAt(mot,cpt)<58 && charAt(mot,cpt)>47)) {
					bonneSaisie = false;
				}
			}
			if (!bonneSaisie) {
				println("Saisie incorrect tu dois saisire un nombre");
				mot = readString();
			}
		}

		int res = 0;
		int cpt2 = length(mot)-1;
		for (int cpt = 0;cpt < length(mot);cpt++ ) {
			res += (((int) charAt(mot,cpt))-48)*pow(10,cpt2);
			cpt2--;
		}
		return res;
	}
	// ********* Fin readInt2() ********	


	String affichage(String mot){
		String res = "";
	
		String[] phrases = coupagePhrase(mot);

		int longueur_max=longueur_max(phrases);

		res += affichage_ligne_pleine(longueur_max);
		res += affichage_ligne_sans_texte(longueur_max);

		for (int cpt = 0;cpt < length(phrases) ;cpt++ ) {
			
			int x = longueur_max+8-length(phrases[cpt])-1;
				for (int cpt2 = 0;cpt2 < x+2 ;cpt2++ ) {
					if (cpt2 == 0 || cpt2 == x+1) {
						res += '*';
					}
					else if (cpt2 == (((int) x/2)+1)){
						res+= phrases[cpt];
					}
					else{
						res+= ' ';
					}
				}			
			res += "\n";
		}	
			

		res += affichage_ligne_sans_texte(longueur_max);
		res += affichage_ligne_pleine(longueur_max);
		
		return res;	
	}

	void testCourpagePhrase(){
		assertArrayEquals(new String[] {"Salut","Ca va ?"},coupagePhrase("Salut\nCa va ?\n"));
		assertArrayEquals(new String[] {"test"},coupagePhrase("test\n"));
	}

	String[] coupagePhrase(String phrases){
		int nbr_de_phrase=0;
		for (int cpt = 0;cpt < length(phrases) ;cpt++ ) {
			if (charAt(phrases,cpt)=='\n') {
				nbr_de_phrase++;
			}
		}
		String[] res = new String[nbr_de_phrase];
		int debut_phrase = 0;
		int nbr_de_phrase_trouve=0;
		for (int cpt = 0;cpt < length(phrases) ;cpt++ ) {
			if(charAt(phrases,cpt)=='\n'){
				res[nbr_de_phrase_trouve]=substring(phrases,debut_phrase,cpt);
				nbr_de_phrase_trouve++;
				debut_phrase = cpt + 1;
			}
		}
		return res;
	}

	void testAffichage_sans_texte(){
		assertEquals("*                 *\n",affichage_ligne_sans_texte(11));
		assertEquals("*      *\n",affichage_ligne_sans_texte(0));
	}

	String affichage_ligne_sans_texte(int longueur_max){
		String res = "";
		for (int cpt = 0;cpt < longueur_max+8 ;cpt++ ) {
			if (cpt == 0 || cpt == longueur_max+7) {
				res += '*';
			}
			else{
				res += ' ';
			}
		}
		res+= "\n";
		return res;	
	}

	void testAffichage_ligne_pleine(){
		assertEquals("*******************\n",affichage_ligne_pleine(11));
		assertEquals("********\n",affichage_ligne_pleine(0));
	}

	String affichage_ligne_pleine(int longueur_max){
		String res = "";
		for (int cpt = 0;cpt < longueur_max+8 ;cpt++ ) {
			res += '*';
		}
		res+= "\n";
		return res;
	}

	void testLongueurMax(){
		assertEquals(12,longueur_max(new String[] {"Salut","Ca va ?","Oui et toi ?"} ));
		assertEquals(1,longueur_max(new String[] {"X"} ));
	}

	int longueur_max(String[] phrases){
		int res=0;
		for(int cpt=0;cpt < length(phrases) ;cpt++ ) {
			int longueur = length(phrases[cpt]);
			res = max(longueur,res);
		}
		return res;
	}


 // ********** PLATEAU DE JEU ********

	String[] plateauAleatoire(){
		String[] res = new String[20];
		res[0] = "J-D";
		res[19] = " A ";
		int cptPendu = 5;
		int cptOperation = 6;
		int cptQuizz = 0;
		int cptHarry = 0;
		int cptBonus = 0;
		int cptMalus = 0;
		for (int cpt = 1;cpt < length(res)-1 ;cpt++ ) {
			if ( ((int) (random()*6)+1) == 1 && cptPendu != 0) {
				res[cpt]=" P ";
				cptPendu--;
			}
			else if (((int) (random()*6)+1) == 2 && cptOperation != 0) {
				res[cpt]=" O ";
				cptOperation--;				
			}
			else if (((int) (random()*6)+1) == 3 && cptQuizz != 0) {
				res[cpt]=" Q ";
				cptQuizz--;				
			}
			else if (((int) (random()*6)+1) == 4 && cptHarry != 0) {
				res[cpt]=" H ";
				cptHarry--;				
			}
			else if (((int) (random()*6)+1) == 5 && cptBonus != 0) {
				res[cpt]=" B ";
				cptBonus--;				
			}
			else if (((int) (random()*6)+1) == 6 && cptMalus != 0) {
				res[cpt]=" M ";
				cptMalus--;				
			}
			else{
				cpt--;
			}												
		}
		return res;
	}

	String[] deplacementAleatoire(String[]tab,int lancer_De){
		String[]tabbis = tab;
		int deAleatoire = lancer_De;
		return changementDePlace(tabbis,deAleatoire);
	}

	void testChangementDeplace(){
		assertArrayEquals(new String[] {" D "," P ","J-R"," B "," A "}
		,changementDePlace(new String[]{" D ","J-P"," R "," B "," A "},1));		
		assertArrayEquals(new String[] {" D "," P "," R "," B ","J-A"}
		,changementDePlace(new String[]{" D "," P "," R ","J-B"," A "},3));
	}

	String[] changementDePlace(String[] tab,int x){
		String[]tabtemp = tab;
		int positionJoueur = indiceJoueur(tabtemp);
		tabtemp[positionJoueur]= " "+charAt(tabtemp[positionJoueur],2)+" ";
		if ( (positionJoueur+x) >= length(tabtemp)) {
			tabtemp[length(tabtemp)-1]= "J-"+ (charAt(tabtemp[length(tabtemp)-1],1));
		}
		else{
			tabtemp[positionJoueur+x]= "J-"+ (charAt(tabtemp[positionJoueur+x],1));
		}
		return tabtemp;
	}

	void testindiceJoueur(){
		assertEquals(0,indiceJoueur(new String[] {"J-D"," P "," Q "}));
		assertEquals(6,indiceJoueur(new String[] {" D "," P "," Q "," M "," O "," R ","J-B"}));
		assertEquals(19,indiceJoueur(new String[] {" D "," P "," Q "," M "," O "," R "," B "," P "," Q "," M "," O "," R "," B "," P "," Q "," M "," O "," R "," B ","J-A"}));
	}

	int indiceJoueur(String[] tab){
		for (int cpt=0;cpt < length(tab) ;cpt++ ){
			if (charAt(tab[cpt],0)=='J'){
				return cpt;
			}
		}
		return 0;
	}

 




	void afficherPlateau(String[] mots, String phrases,Joueur j){
		text("cyan");
		println("                      Tu as "+j.points+" point(s)                         ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("*  "+mots[0]+"  * "+"*  "+mots[1]+"  * "+"*  "+mots[2]+"  * "+"*  "+mots[3]+"  * "+"*  "+mots[4]+"  * "+"*  "+mots[5]+"  * ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		println(""+                                    "                                                  *********");
		println(""+                                    "                                                  *       *");
		println(""+                                    "                                                  *  "+mots[6]+"  *");
		println(""+                                    "                                                  *       *");
		println(""+                                    "                                                  ********* "                                +"      D = Départ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "          "  +   "********* "                                +"      P = Jeu du pendu");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "          "  +   "*       * "                                +"      Q = Jeu money-drop ( quizz )");
		println("*  "+mots[16]+"  * "+"*  "+mots[17]+"  * "+"*  "+mots[18]+"  * "+"*  "+mots[19]+"  * "  +      "          "+"*  "+mots[7]+"  * "    +"      J = Place du joueur");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "          "  +   "*       * "                                +"      O = Operation à trou");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "          "  +   "********* "                                +"      R = Re-fait le mot");
		println("********* " +   "          "  +   "          "  +   "          "  +   "          "  +   "********* "                                +"      B = Bonus");
		println("*       * " +   "          "  +   "          "  +   "          "  +   "          "  +   "*       * "                                +"      M = malus");
		println("*  "+mots[15]+"  * "+"   "+"    "+"   "+               "    "+"   "+"    "+"               "+"    "+"*  "+mots[8]+"  * "			 +"      A = Arrivé");
		println("*       * " +   "          "  +   "          "  +   "          "  +   "          "  +   "*       * ");
		println("********* " +   "          "  +   "          "  +   "          "  +   "          "  +   "********* ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("*  "+mots[14]+"  * "+"*  "+mots[13]+"  * "+"*  "+mots[12]+"  * "+"*  "+mots[11]+"  * "+"*  "+mots[10]+"  * "+"*  "+mots[9]+"  * ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		print("\n");
		reset();
		if (!(equals(phrases," "))) {
			print(affichage(phrases));				
		}					
	}

	//****** Fin gestion plateau ******

	//***** Début lancer du dé *******

	int lancerDuDe(String[]plateau){
		int cpt2 = 1;
		for (int cpt = 20;cpt < 400 ;cpt = cpt + 15 ) {
			if (cpt2 == 7) {
				cpt2 = 1;
			}
			afficherPlateau(plateau," ",joueur);
			println(generateur_De(cpt2));
			delay(cpt);
			clearScreen();
			cursor(1,1);
			cpt2++;
		}
		int nbrAlea = (int) (random()*3)+1;
		afficherPlateau(plateau," ",joueur);
		println(generateur_De(nbrAlea));
		print("                        Tu as fait "+nbrAlea+".\n                Appuie sur entre pour avancer\n");
		readString();
		return nbrAlea;
	}

	String generateur_De(int nbr){
		switch(nbr){
			case 1:
				return "                       *************\n                       *           *\n                       *           *\n                       *     *     *\n                       *           *\n                       *           *\n                       *************\n";
			case 2:
				return "                       *************\n                       *           *\n                       *   *       *\n                       *           *\n                       *       *   *\n                       *           *\n                       *************\n";
			case 3:
				return "                       *************\n                       *     *     *\n                       *           *\n                       *     *     *\n                       *           *\n                       *     *     *\n                       *************\n";
			case 4:
				return "                       *************\n                       *           *\n                       *   *   *   *\n                       *           *\n                       *   *   *   *\n                       *           *\n                       *************\n";
			case 5:
				return "                       *************\n                       *           *\n                       *   *   *   *\n                       *     *     *\n                       *   *   *   *\n                       *           *\n                       *************\n";
			case 6:
				return "                       *************\n                       *           *\n                       *  *     *  *\n                       *  *     *  *\n                       *  *     *  *\n                       *           *\n                       *************\n";
			default:
				return "ERREUR";
		}
	}
	// ******** Fin de la gestion du plateau ******

	// ******** Gestion du CSV des scores *******
	void ajouterInfoCSV(CSVFile test,Joueur joueur,String nomDuCSV){

		String[][] tab = new String[rowCount(test)+1][columnCount(test)];
		for (int cpt = 0;cpt<rowCount(test) ;cpt++ ) {
			for (int cpt2 = 0;cpt2<columnCount(test);cpt2++) {
				tab[cpt][cpt2] = getCell(test,cpt,cpt2);
			}
		}
		tab[rowCount(test)][0]=joueur.nom;
		tab[rowCount(test)][1]=joueur.points+"";
		saveCSV(tab,nomDuCSV,';');
	}

	String generationClassement(CSVFile classement){
		String res = "";
		for (int cpt = 0;cpt<rowCount(classement) ;cpt++ ) {
				res += getCell(classement,cpt,0)+" - "+getCell(classement,cpt,1)+" \n";
		}
		return res;
	}
}


