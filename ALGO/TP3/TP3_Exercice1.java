class TP3_Exercice1 extends Program {
    void testSomme() {
	assertEquals(1, somme(1));
	assertEquals(3, somme(2));
	assertEquals(6, somme(3));
	assertEquals(10, somme(4));
    }
    int somme(int c) {
	int somme = 0;
	for(int cpt = 0; cpt <= c ; cpt = cpt + 1){

	    somme = somme + cpt;
	}
	return somme;
    }
}
