class TP3_Exercice3 extends Program {
    void algorithm() {
	print("Combien de lignes ? ");
	int ligne = readInt();
	print("A partir de ? ");
	int debut = readInt();
	double resultat;
	for(double cpt = debut; cpt < (ligne/2 + debut); cpt = cpt + 0.5){
	    resultat = cpt * 1.196;
	    println( cpt + "euros HT = " + resultat + "euros TTC.");
	}
    }
}
