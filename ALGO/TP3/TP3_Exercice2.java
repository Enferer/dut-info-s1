class TP3_Exercice2 extends Program {
    void testRepeterFor() {
	assertEquals("hello\n", repeterFor("hello", 1));
	assertEquals("hello\nhello\n", repeterFor("hello", 2));
	assertEquals("hello\nhello\nhello\n", repeterFor("hello", 3));
	assertEquals("hello\nhello\nhello\nhello\n", repeterFor("hello", 4));
    }
    String repeterFor(String mot, int c) {
	String resultat = "";
	for( int cpt = 0; cpt < c; cpt = cpt + 1){
	    resultat = resultat + mot + "\n";
	}
	return resultat;
    }
    void testRepeterWhile() {
	assertEquals("hello\n", repeterFor("hello", 1));
	assertEquals("hello\nhello\n", repeterFor("hello", 2));
	assertEquals("hello\nhello\nhello\n", repeterFor("hello", 3));
	assertEquals("hello\nhello\nhello\nhello\n", repeterFor("hello", 4));
    }
    String repeterWhile(String mot, int c) {
	String resultat = "";
	int cpt = 0;
	while(cpt < c){
	    cpt = cpt + 1;
	    resultat = resultat + mot + "\n";
	}
	return resultat;
    }
}
