class TP3_Exercice5 extends Program {
    void testOccurrence() {
	String phrase = "Hello World !";
	assertEquals(2, occurrenceFor(phrase, 'o'));
	assertEquals(3, occurrenceFor(phrase, 'l'));
	assertEquals(0, occurrenceFor(phrase, 'y'));
    }
    int occurrenceFor(String phrase,char x) {
	int longueur = length(phrase);
	char lettre;
	int resultat=0;
       	for(int cpt = 0;cpt < longueur; cpt++){
	    lettre = charAt(phrase, cpt);
	    if(lettre == x){
		resultat++;
	    }	    
	}
	return(resultat);
    }
    void testOccurrenceWhile() {
	String phrase = "Hello World !";
	assertEquals(2, occurrenceFor(phrase, 'o'));
	assertEquals(3, occurrenceFor(phrase, 'l'));
	assertEquals(0, occurrenceFor(phrase, 'y'));
    }
     int occurrenceWhile(String phrase,char x) {
	int longueur = length(phrase);
	char lettre;
	int cpt = 0;
	int resultat=0;
       	while(cpt < longueur){
	    cpt = cpt +1;
	    lettre = charAt(phrase, cpt);
	    if(lettre == x){
		resultat++;
	    }	    
	}
	return(resultat);
    }
    
}
