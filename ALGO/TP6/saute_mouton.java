class saute_mouton extends Program{
	final char MOUTONGAUCHE = '>';
	final char MOUTONDROITE = '<';
	final char VIDE = '_';
	final String INITIAL = ">>>_<<<";
	final String FINAL = "<<<_>>>";

	/*void testToString(){
		assertEquals(INITIAL, toString(new char[]{'>','>','>','_','<','<','<'}));
	}
	void testInitialiser(){
		assertEquals(INITIAL, initialiser());
	}
	void testEstValide(){
		assertFalse(estValide(-1));
		assertTrue(estValide(5));
		assertFalse(estValide(7));
	}
	void testAvancer(){
		assertTrue(avancer(INITIAL,2));
		assertFalse(avancer(INITIAL,1));
	}
	void testSauter(){
		assertTrue(sauter(INITIAL,1));
		assertTrue(sauter(INITIAL,5));
		assertFalse(sauter(INITIAL,0));
		assertFalse(sauter(INITIAL,6));
	}
	void testVictoire(){
		assertTrue(victoire(FINAL));
		assertFalse(victoire(INITIAL));
	}
	void testBloque(){
		assertFalse(bloque(INITIAL));
		assertTrue(bloque(>><><<_));
	}*/


	/////////////////////

	String toString(char[]prairie){
		String visualisation = "";
		for (int i=0; i<length(prairie); i++) {
			visualisation += prairie[i];
		}
		return visualisation;
	}

	////////////////////

	String initialiser(){
		return(INITIAL);
	}

	////////////////////

	boolean estValide(int x){
		if(x>=0 && x<7){
			return true;
		}
	return false;
	}

	/////////////////////

	int saisie(){
		int saisie = readInt();
		while(!estValide(saisie)){
			println("Nombre invalide");
			saisie = readInt();
		}
		return saisie;
	}

	/////////////////////
	char[] toTab (String plateau){
		char[] plat = new char[length(plateau)];
		for (int cpt = 0; cpt < length(plateau); cpt++){
			plat[cpt] = charAt(plateau, cpt);
		}
		return plat; 
	}

	boolean avancer(String plateau,int mouton){
		char[] plat = toTab(plateau);
		if (!(plat[mouton+1]=='_')) {
			return false;
		}
		if (plat[mouton]=='>') {
			plat[mouton+1]=plat[mouton];
		}
		if (plat[mouton]=='<') {
			plat[mouton-1]=plat[mouton];
		}
		plat[mouton]='_';
		plateau = toString(plat);
		return true;
	}
	///////////////////
	boolean sauter(String plateau,int mouton){
		char[] plat = toTab(plateau);
		if (charAt(plateau, mouton)=='>'){
			if (charAt(plateau,mouton + 2) == '_'){
				plat[mouton+2]=plat[mouton];
				plat[mouton]='_';
				plateau = toString(plat);
				return true;
			}
			else {
				return false;
			}		
		}
		if (charAt(plateau, mouton)=='<'){	
			if (charAt(plateau,mouton - 2) == '_'){
				plat[mouton-2]=plat[mouton];
				plat[mouton]='_';
				plateau = toString(plat);
				return true;
			}
			else {
				return false;		
			}
		}
		return false;
	}
	////////////////////////////////////
	boolean victoire (String plateau){
		if (plateau == FINAL) {
			return true;
		}
		return false;
	}
	/////////////////////////////
	boolean bloque(String plateau){
		char[]tab = toTab(plateau);
		int x = 0;
		for (int cpt = 0;cpt < 7 ;cpt++ ) {	
			if (charAt(plateau,cpt)=='_') {
				x++;
			}			
			else if (!(sauter(plateau,cpt) || avancer(plateau,cpt))){
				x++;
			}
		}
		if (x == 7) {
			return true;
		}
		return false;
	}
	//////////////////////////////

	void algorithm(){
		String plateau = INITIAL;
		while(!victoire(plateau) && !bloque(plateau)){
			println("saisie le mouton que tu veux déplacer:");
			int mouton = readInt();
			while(mouton < 0 && mouton >6){
				mouton = readInt();
			}
			println("Veux tu sauter ou avancer?");
			String action = readString();
			if (action == "sauter"){
				while(!sauter(plateau,mouton)){
					println("Impossible resaisie");
					mouton = readInt();
				}
			}
			else if(action == "avancer"){
				while(!avancer(plateau,mouton)){
					println("Impossible resaisie");
					mouton = readInt();					
				}
			}
			println(plateau);
		}
	}
}