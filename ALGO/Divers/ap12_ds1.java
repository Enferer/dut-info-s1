class ap12_ds1 extends Program{
	
	String voyelles() {
	  return "aeouiy";
	}
	String consonnes() {
  		return "bcdfghjklmnpqrstvwxz";
	}
	

	void testEstLettre() {
    	assertEquals(true, estLettre('a'));
   	 	assertEquals(true, estLettre('z'));
    	assertEquals(true, estLettre('i'));
    	assertEquals(false, estLettre('-'));
    	assertEquals(false, estLettre('{'));
  	}
	boolean estLettre(char caractere){
		if(caractere >= 'a' && caractere <= 'z'){
			return true;
		}
		return false;
	}
	void testIndice() {
    	assertEquals(1, trouverIndice("aeouiy",'e'));
    	assertEquals(18, trouverIndice("bcdfghjklmnpqrstvwxz", 'x')); 
    	assertEquals(-1, trouverIndice("aeouiy", 'x')); 
    }
    int trouverIndice(String mot,char lettre){
		for(int cpt = 0; cpt < length(mot); cpt++){
			if(charAt(mot,cpt) == lettre){
				return cpt;
			}
		}
		return -1;
	}
	void testContient() {
    	assertTrue(contient("aeouiy",'e'));
    	assertFalse(contient("aeouiy", 'x')); 
    	assertTrue(contient("bcdfghjklmnpqrstvwxz", 'x')); 
  	}
  	
  	boolean contient(String s,char car){
  		boolean present = false;
  		int indice = 0;
  		for(int cpt = 0; cpt < length(s); cpt++){
  			if (charAt(s,cpt)== car) {
  				present = true;
  			}
  		}
  		return present;
  	}
    void testEstVoyelle() {
    	assertTrue(estVoyelle('y'));
    	assertFalse(estVoyelle(',')); 
    	assertFalse(estVoyelle('x')); 
  	}
  	boolean estVoyelle(char caractere){
  		for(int cpt = 0; cpt < 6;cpt++){
  			if(caractere == charAt(voyelles(),cpt)){
  				return true;
  			}
  		}
  		return false;
  	}
  	void testEstConsonne() {
    	assertTrue(estConsonne('z'));
    	assertFalse(estConsonne('!')); 
    	assertFalse(estConsonne('a')); 
  }
  	boolean estConsonne(char caractere){
  		for(int cpt = 0; cpt < 20;cpt++){
  			if(caractere == charAt(consonnes(),cpt)){
  				return true;
  			}
  		}
  		return false;
  	}
}

