import extensions.*; 
class Testimage extends Program{
	TransparentImage img; 
	void algorithm() { 
		final int LIGNES = 10, COLONNES = 10; 
		final int HAUTEUR = 800, LARGEUR = 1200;  
		int hauteurZone = HAUTEUR/LIGNES; 
		int largeurZone = LARGEUR/COLONNES; 
		img = newTransparentImage(LARGEUR, HAUTEUR);  
		setGrid(img, LIGNES, COLONNES);  
		show(img); 
		for (int lig=0; lig<LIGNES; lig++) { 
			for (int col=0; col<COLONNES; col++) { 
				int origineX = col*largeurZone; 
				int origineY = lig*hauteurZone; 
				addZone(img, "("+lig+","+col+")", origineX, origineY, largeurZone, hauteurZone); 
				drawRect(img, origineX, origineY, largeurZone, hauteurZone);  
			} 
		}

		while(true){
			for (int lig=0; lig<LIGNES; lig++) { 
				for (int col=0; col<COLONNES; col++) { 
					
					set(img, lig,col, randomColor());
				} 
		    }
		    delay(100);
		    //readString();
		} 
  	} 
	/*void mouseChanged(String name, int x, int y, int button, String event) { 
		println("Zone "+name+" a genere l'evenement "+event); 
		if (equals(event, "CLICKED")) { 
			set(img, charAt(name,1)-'0', charAt(name,3)-'0', randomColor());  
		} 
	}*/
}