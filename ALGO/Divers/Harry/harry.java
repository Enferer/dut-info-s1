class harry extends Program{
	
	//2 essais possible 
	final String MOT_A_TROUVER = "arbre";
	
	//tronquer le mot en 2 ou 3 morceaux
	String tronquer(String MOT_A_TROUVER){

		String res = "";
		int alea = (int)(random()*2);
		if (length(MOT_A_TROUVER)== 8) {
			if (alea == 0) {
				int couper = (int)(length(MOT_A_TROUVER)/2);
				String mot_part1 = substring(MOT_A_TROUVER,0,couper);
				String mot_part2 = substring(MOT_A_TROUVER,couper,length(MOT_A_TROUVER));
				res =  mot_part2 +"-"+ mot_part1; 
			}else{
				int couper = (int)(length(MOT_A_TROUVER)/3);
				String mot_part1 = substring(MOT_A_TROUVER,0,couper+1);
				String mot_part2 = substring(MOT_A_TROUVER,couper+1,length(MOT_A_TROUVER)-couper);
				String mot_part3 = substring(MOT_A_TROUVER,couper+couper+2,length(MOT_A_TROUVER));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
			}
		}
		else if (length(MOT_A_TROUVER)==7) {
			if (alea == 0) {
				int couper = (int)(length(MOT_A_TROUVER)/2);
				String mot_part1 = substring(MOT_A_TROUVER,0,couper);
				String mot_part2 = substring(MOT_A_TROUVER,couper,length(MOT_A_TROUVER));
				res =  mot_part2 +"-"+ mot_part1; 
			}else{
				int couper = (int)(length(MOT_A_TROUVER)/3);

				String mot_part1 = substring(MOT_A_TROUVER,0,couper+1);
				String mot_part2 = substring(MOT_A_TROUVER,couper+1,length(MOT_A_TROUVER)-couper);
				String mot_part3 = substring(MOT_A_TROUVER,couper+couper+1,length(MOT_A_TROUVER));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
			}
		}
		else if (length(MOT_A_TROUVER)==6) {
			if (alea == 0) {
				int couper = (int)(length(MOT_A_TROUVER)/2);
				String mot_part1 = substring(MOT_A_TROUVER,0,couper);
				String mot_part2 = substring(MOT_A_TROUVER,couper,length(MOT_A_TROUVER));
				res =  mot_part2 +"-"+ mot_part1; 
			}else{
				int couper = (int)(length(MOT_A_TROUVER)/3);

				String mot_part1 = substring(MOT_A_TROUVER,0,couper);
				String mot_part2 = substring(MOT_A_TROUVER,couper,length(MOT_A_TROUVER)-2);
				String mot_part3 = substring(MOT_A_TROUVER,couper+couper,length(MOT_A_TROUVER));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
			}
		}
		else if (length(MOT_A_TROUVER)==5) {
			if (alea == 0) {
				int couper = (int)(length(MOT_A_TROUVER)/2);
				String mot_part1 = substring(MOT_A_TROUVER,0,couper);
				String mot_part2 = substring(MOT_A_TROUVER,couper,length(MOT_A_TROUVER));
				res =  mot_part2 +"-"+ mot_part1; 
			}else{
				int couper = (int)(length(MOT_A_TROUVER)/3);
				String mot_part1 = substring(MOT_A_TROUVER,0,couper+1);
				String mot_part2 = substring(MOT_A_TROUVER,couper+1,length(MOT_A_TROUVER)-couper);
				String mot_part3 = substring(MOT_A_TROUVER,length(MOT_A_TROUVER)-couper,length(MOT_A_TROUVER));
				int aleat = (int)(random()*3);
				if (aleat==0) {
					res = mot_part3 +"-"+ mot_part1 +"-"+ mot_part2;
				}else if (aleat==1) {
					res = mot_part1 +"-"+ mot_part3 +"-"+ mot_part2;
				}else{
					res = mot_part2 +"-"+ mot_part1 +"-"+ mot_part3;
				}
			}
		}
		return res;
	}

	void algorithm(){
	
	/*piocher un mot aléatoirement
	CSVFile motHarry = loadCSV("mots_harry.csv",';');
	int idxMot = (int)(random()*rowCount(motHarry));
	String mot_a_trouver = getCell(motHarry,idxMot,0);
	*/
		
		boolean jeufini = false;
		for (int cpt=0;cpt<5;cpt++) {
			println("Mot à trouver : "+tronquer(MOT_A_TROUVER));
			int essais = 1;
			while(!jeufini){
				String reponseU = readString();
				reponseU = toLowerCase(reponseU);
				if(equals(reponseU,MOT_A_TROUVER)){
					println("Le mot était : "+MOT_A_TROUVER+", Bien joué !!!");
					jeufini = true;
				} else if(essais != 0) {
					println("Il te reste "+essais+" essai.");
					println("Mot à trouver :"+tronquer(MOT_A_TROUVER));
					essais--;
				} else{
					println("Dommage !!! Le mot à trouver était : "+MOT_A_TROUVER+".");
					jeufini = true;
				}
			}
			jeufini = false;
		}
	}
}

// 6l cactus   -> ca|ct|us   ou cac|tus
// 5l arbre    -> ar|br|e    ou arb|re
// 7l maisons  -> ma|is|ons  ou mais|ons
// 8l tronquer -> tro|nqu|er ou tron|que 