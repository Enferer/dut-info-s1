class Classement extends Program{
	
	void algorithm(){
		String tab[][] = new String[][]{{"Thibaut","20"},{"Amélie","50"},{"jean-claude","25"}}; 
 
		tab = triBulleCroissant(tab);

		for (int cpt = 0;cpt < length(tab,1) ;cpt++ ) {
			for (int cpt2 = 0;cpt2 < length(tab,2);cpt2++) {
				print(tab[cpt][cpt2]+' ');
			}
			print('\n');
		}
 	}
 
	String[][] triDecroissant(String[][] tab) {
		int longueur = length(tab);
		String temp = "";
		boolean trie;
		String[][]tableau = tab;
 
		do {
			trie = false;
			for (int cpt = 0; cpt < longueur - 1; cpt++) {
				
				if (stringToInt(tableau[cpt][1]) < stringToInt(tableau[cpt+1][1])) {

					temp = tableau[cpt][1];
					tableau[cpt][1] = tableau[cpt + 1][1];
					tableau[cpt + 1][1] = temp;
				
					temp = tableau[cpt][0];
					tableau[cpt][0] = tableau[cpt + 1][0];
					tableau[cpt + 1][0] = temp;

					trie = true;
				}
			}
		} while (trie);

		return tableau;
	}

}