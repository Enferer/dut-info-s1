class harry extends Program{
	
	//2 essais possible 
	final String MOT_A_TROUVER = "cactus";
	
	//tronquer le mot en 2 ou 3 morceaux
	String tronquer(String MOT_A_TROUVER){

		String res = "";

		if (random()>0.5) {
			int couper = (int)(length(MOT_A_TROUVER)/2);
			String mot_part1 = substring(MOT_A_TROUVER,0,couper);
			String mot_part2 = substring(MOT_A_TROUVER,couper,length(MOT_A_TROUVER));
			res =  mot_part2 + "-"+ mot_part1; 
		}
		else{
			int couper = (int)(length(MOT_A_TROUVER)/3);
			String mot_part1 = substring(MOT_A_TROUVER,0,couper);
			String mot_part2 = substring(MOT_A_TROUVER,couper,couper+2);
			String mot_part3 = substring(MOT_A_TROUVER,couper+couper,length(MOT_A_TROUVER));
			res = mot_part3 + "-" +mot_part1 +"-"+ mot_part2;
		}
		
		return res;
	}

	void algorithm(){
	
	/*piocher un mot aléatoirement
	CSVFile motHarry = loadCSV("mots_harry.csv",';');
	int idxMot = (int)(random()*rowCount(motHarry));
	String mot_a_trouver = getCell(motHarry,idxMot,0);
	*/
		println("Mot à trouver :"+tronquer(MOT_A_TROUVER));
		String reponseU = readString();
		if(reponseU != MOT_A_TROUVER){
			println("C'est ton dernier essai !!!");
			println("Mot à trouver :"+tronquer(MOT_A_TROUVER));
		} else {

			println("Bien joué !!!");
		}
	}
}

// 6l cactus   -> ca|ct|us   ou cac|tus
// 5l arbre    -> ar|br|e    ou arb|re
// 7l maisons  -> ma|is|ons  ou mais|ons
// 8l tronquer -> tro|nqu|er ou tron|quer