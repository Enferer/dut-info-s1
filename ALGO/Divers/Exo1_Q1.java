class Exo1_Q1 extends Program{
	
	void testEstLettre() {
		assertFalse( estLettre('Z'));
		assertTrue ( estLettre('a'));
		assertTrue ( estLettre('v'));
		assertFalse( estLettre('!'));
	}

	boolean estLettre(char caractere){
		if(caractere >= 'a' && caractere <= 'z'){
			return true;
		}
		return false;
	}
	void testPositionLettre() {
		assertEquals( -1, positionLettre('c', "azerty"));
		assertEquals( 3, positionLettre('r', "azerrty"));
	}
	int positionLettre(char lettre, String mot){
		for(int cpt = 0; cpt < length(mot); cpt++){
			if(charAt(mot,cpt) == lettre){
				return cpt;
			}
		}
		return -1;
	}
	void testSupprimeLettre() {
		assertEquals( "onjour", supprimeLettre(0, "bonjour"));
		assertEquals( "onour", supprimeLettre(2, "onjour"));
	}

	String supprimeLettre(int indice,String mot){
		if(indice == 0){
			return(substring(mot,1,length(mot)));
		}
		else{
			String reponse = "";
			reponse = substring(mot,0,indice)+substring(mot,indice+1,length(mot));
			return reponse;
		}
	}
	void testGenereAlphabet() {
		assertEquals("abcdefghijklmnopqrstuvwxyz", genereAlphabet());
	}
	String genereAlphabet(){
		String resultat = "";

		for (int cpt=97 ; cpt < 123 ; cpt++ ) {
			resultat = resultat + (char)cpt;
		}
		return resultat;
	}		

	void testEstPangramme() {
		assertTrue( estPangramme("portez ce vieux whisky au juge blond qui fume"));
		assertTrue( estPangramme("monsieur jack vous dactylographiez bien mieux que votre ami wolf"));
		assertTrue( estPangramme("buvez de ce whisky que le patron juge fameux"));
	}

	boolean estPangramme(String phrase){
		
		for(int cpt = 0; cpt < length(phrase)-1 ; cpt--){
			if(!estLettre(charAt(phrase,cpt))){
				supprimeLettre(cpt,phrase);
			}

		}
		println("phrase");
		return true;
	}
}
