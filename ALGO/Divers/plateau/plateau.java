class plateau extends Program{
	void algorithm(){
		clearScreen();
		String[] cases = {"J-D"," P "," Q "," M "," O "," R "," B "," P "," Q "," M "," O "," R "," M "," P "," Q "," M "," O "," R "," B "," A "};
		String test = " Salut ca va ?";
		afficherPlteau1(cases,test);
		while(true){
			cases = deplacementAleatoire(cases);
			afficherPlteau1(cases,test);
			readString();
		}
	}
	
	String[] deplacementAleatoire(String[]tab){
		String[]tabbis = tab;
		int de = (int) (random()*3)+1;
		return changementDePlace(tabbis,de);
	}

	String[] changementDePlace(String[] tab,int x){
		String[]tabtemp = tab;
		int positionJoueur = indiceJoueur(tabtemp);
		tabtemp[positionJoueur]= " "+charAt(tabtemp[positionJoueur],2)+" ";
		if ( (positionJoueur+x) >= length(tabtemp)) {
			tabtemp[length(tabtemp)-1]= "J-"+ (charAt(tabtemp[length(tabtemp)-1],1));
		}
		else{
			tabtemp[positionJoueur+x]= "J-"+ (charAt(tabtemp[positionJoueur+x],1));
		}
		return tabtemp;
	}

	int indiceJoueur(String[] tab){
		for (int cpt=0;cpt < length(tab) ;cpt++ ){
			if (charAt(tab[cpt],0)=='J') {
				return cpt;
			}
		}
		return 0;
	}






	void afficherPlteau1(String[] mots, String mot){
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("*  "+mots[0]+"  * "+"*  "+mots[1]+"  * "+"*  "+mots[2]+"  * "+"*  "+mots[3]+"  * "+"*  "+mots[4]+"  * "+"*  "+mots[5]+"  * ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		println(""+                                    "                                                  *********");
		println(""+                                    "                                                  *       *");
		println(""+                                    "                                                  *  "+mots[6]+"  *");
		println(""+                                    "                                                  *       *");
		println(""+                                    "                                                  ********* "                                +"      D = Départ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "          "  +   "********* "                                +"      P = Jeu du pendu");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "          "  +   "*       * "                                +"      Q = Jeu money-drop ( quizz )");
		println("*  "+mots[16]+"  * "+"*  "+mots[17]+"  * "+"*  "+mots[18]+"  * "+"*  "+mots[19]+"  * "  +      "          "+"*  "+mots[7]+"  * "    +"      J = Place du joueur");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "          "  +   "*       * "                                +"      O = Operation à trou");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "          "  +   "********* "                                +"      R = Re-fait le mot");
		println("********* " +   "          "  +   "          "  +   "          "  +   "          "  +   "********* "                                +"      B = Bonus");
		println("*       * " +   "          "  +   "          "  +   "          "  +   "          "  +   "*       * "                                +"      M = malus");
		println("*  "+mots[15]+"    "+"   "+"    "+"   "+               "    "+"   "+"    "+"               "+"    "+"*  "+mots[8]+"  * "			 +"      A = Arrivé");
		println("*       * " +   "          "  +   "          "  +   "          "  +   "          "  +   "*       * ");
		println("********* " +   "          "  +   "          "  +   "          "  +   "          "  +   "********* ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("*  "+mots[14]+"  * "+"*  "+mots[13]+"  * "+"*  "+mots[12]+"  * "+"*  "+mots[11]+"  * "+"*  "+mots[10]+"  * "+"*  "+mots[9]+"  * ");
		println("*       * " +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * "  +   "*       * ");
		println("********* " +   "********* "  +   "********* "  +   "********* "  +   "********* "  +   "********* ");
		print("\n");
		for (int cpt = 0;cpt < 60 ;cpt++ ) {
			if (cpt == (int) ((60-length(mot))/2)) {
				print(mot);
			}
			else{
				print(' ');
			}

		}
		print("\n");
	}
}