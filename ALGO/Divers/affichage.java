class affichage extends Program{
	void algorithm(){
		/*String res = "";
		int nbr_de_ligne = readInt();
		String[] phrases = new String[nbr_de_ligne];
		for (int cpt = 1;cpt <= nbr_de_ligne ;cpt++ ) {
			println("Saisie la phrase n°"+cpt);
			phrases[cpt-1]=readString();
		}*/
		String res = "";
		final String PHRASE = "Salut\nCa va ?\nMoi oué tranquil\n";
		
		String[] phrases = coupagePhrase(PHRASE);

		int longueur_max=longueur_max(phrases);

		res += affichage_ligne_pleine(longueur_max);
		res += affichage_ligne_sans_texte(longueur_max);

		for (int cpt = 0;cpt < length(phrases) ;cpt++ ) {
			
			int x = longueur_max+8-length(phrases[cpt])-1;
			
			for (int cpt2 = 0;cpt2 < x+2 ;cpt2++ ) {
				if (cpt2 == 0 || cpt2 == x+1) {
					res += '*';
				}
				else if (cpt2 == (((int) x/2)+1)){
					res+= phrases[cpt];
				}
				else{
					res+= ' ';
				}
			}
			res += "\n";
		}

		res += affichage_ligne_sans_texte(longueur_max);
		res += affichage_ligne_pleine(longueur_max);
		
		print(res);		
	}

	String[] coupagePhrase(String phrases){
		int nbr_de_phrase=0;
		for (int cpt = 0;cpt < length(phrases) ;cpt++ ) {
			if (charAt(phrases,cpt)=='\n') {
				nbr_de_phrase++;
			}
		}
		String[] res = new String[nbr_de_phrase];
		int debut_phrase = 0;
		int nbr_de_phrase_trouve=0;
		for (int cpt = 0;cpt < length(phrases) ;cpt++ ) {
			if(charAt(phrases,cpt)=='\n'){
				res[nbr_de_phrase_trouve]=substring(phrases,debut_phrase,cpt);
				nbr_de_phrase_trouve++;
				debut_phrase = cpt + 1;
			}
		}
		return res;
	}


	String affichage_ligne_sans_texte(int longueur_max){
		String res = "";
		for (int cpt = 0;cpt < longueur_max+8 ;cpt++ ) {
			if (cpt == 0 || cpt == longueur_max+7) {
				res += '*';
			}
			else{
				res += ' ';
			}
		}
		res+= "\n";
		return res;	
	}
	String affichage_ligne_pleine(int longueur_max){
		String res = "";
		for (int cpt = 0;cpt < longueur_max+8 ;cpt++ ) {
			res += '*';
		}
		res+= "\n";
		return res;
	}


	int longueur_max(String[] phrases){
		int res=0;
		for(int cpt=0;cpt < length(phrases) ;cpt++ ) {
			int longueur = length(phrases[cpt]);
			res = max(longueur,res);
		}
		return res;
	}
}