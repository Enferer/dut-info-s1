class de extends Program{
	void algorithm(){
		int cpt2 = 1;
		for (int cpt = 20;cpt < 400 ;cpt = cpt + 15 ) {
			if (cpt2 == 7) {
				cpt2 = 1;
			}
			println(generateur_De(cpt2));
			delay(cpt);
			clearScreen();
			cursor(1,1);
			cpt2++;
		}
		println(generateur_De((int) (random()*6)+1));
	}

	String generateur_De(int nbr){
		switch(nbr){
			case 1:
				return "*************\n*           *\n*           *\n*     *     *\n*           *\n*           *\n*************\n";
			case 2:
				return "*************\n*           *\n*   *       *\n*           *\n*       *   *\n*           *\n*************\n";
			case 3:
				return "*************\n*     *     *\n*           *\n*     *     *\n*           *\n*     *     *\n*************\n";
			case 4:
				return "*************\n*           *\n*   *   *   *\n*           *\n*   *   *   *\n*           *\n*************\n";
			case 5:
				return "*************\n*           *\n*   *   *   *\n*     *     *\n*   *   *   *\n*           *\n*************\n";
			case 6:
				return "*************\n*           *\n*  *     *  *\n*  *     *  *\n*  *     *  *\n*           *\n*************\n";
			default:
				return "ERREUR";
		}
	}
}

