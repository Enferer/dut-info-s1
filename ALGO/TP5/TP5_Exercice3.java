class TP5_Exercice3 extends Program {
	
	void testReadVector() {
		assertArrayEquals(new int[]{1,2,3}, readVector());
	}

	int[] readVector(){
		int[]tabvector = new int[3];
		for (int cpt = 0; cpt < 3 ; cpt++ ) {
			int x = readInt();
			tabvector[cpt]=x;
		}
		return tabvector;
	}

	void testVectorToString() {
		int[] v1 = new int[]{1,2,3};
		assertEquals("[1,2,3]", vectorToString(v1));
	}

	String vectorToString(int[]tab){
		String reponse = "[";
		for (int cpt= 0 ;cpt < length(tab); cpt++ ) {
			reponse = reponse + tab[cpt];
			if (cpt < length(tab)-1) {
				reponse = reponse + ',';
			}
		}
		reponse = reponse + ']';
		return reponse;
	}

	void testAdditionner() {
		int[] v1 = new int[]{1,2,3}, v2 = new int[]{-1,-2,-3};
		assertArrayEquals(new int[]{0,0,0}, add(v1,v2));
	}

	int[] add(int[]tab1,int[]tab2){
		int[] tabreponses = new int[3];
		for (int cpt = 0;cpt < 3 ;cpt++ ) {
			tabreponses[cpt]=tab2[cpt]+tab1[cpt];
		}
		return tabreponses;
	}

	void testScalarProduct() {
		assertEquals(6, scalarProduct(new int[]{1,1,1},new int[]{1,2,3}));
		assertEquals(-14, scalarProduct(new int[]{1,2,3},new int[]{-1,-2,-3}));
	}

	int scalarProduct(int[]tab1,int[]tab2){
		return (tab1[0]*tab2[0] + tab1[1]*tab2[1] + tab1[2]*tab2[2]);

	}


	void testEquals() {
		assertTrue(equals(new int[]{1,1,1}, new int[]{1,1,1}));
		assertFalse(equals(new int[]{1,1,2}, new int[]{1,1,1}));
	}

	boolean equals(int[]tab1,int[]tab2){
		int x = 0;
		for (int cpt=0;cpt < length(tab2);cpt++ ) {
			if (tab1[cpt]==tab2[cpt]) {
				x++;
			}
		}
		if (x == length(tab1)) {
			return true;
		}
		return false;
	}
}