class lavie extends Program{

	void algorithm(){
		int longueur = readInt();
		int hauteur = readInt();
		char[][] tab= creerplateaualeatoire(longueur,hauteur);
		boolean fini = false;


		while(!fini){
			affiche(tab);
			tab = prochainegeneration(tab);
			delay(200);
			clearScreen();
			cursor(1,1);
		}
	}


	
	char [][] prochainegeneration(char[][]tab){
		char[][] tabinter = new char[length(tab,1)][length(tab,2)];
		for (int l =0; l < length(tab,1) ; l++ ) {
			for (int c = 0; c < length(tab,2) ; c++ ) {
					if (!(c==0 || c == length(tab,2)-1 || l==0 || l == length(tab,1)-1)) {
						if (compter(tab,l,c)==3) {
							tabinter[l][c]='O';
						}
						else if(compter(tab,l,c)==2){
							if (tab[l][c]=='O') {
								tabinter[l][c]='O';
							}
							else{tabinter[l][c]='*';}
						}
						else{tabinter[l][c]='*';}
					}
					else{tabinter[l][c]='*';}
				}
		}
		return tabinter;
	}

	int compter(char[][]tab,int l, int c){
		int compte = 0;
			if (tab[(l-1)][(c-1)]=='O'){compte++;}
			if (tab[(l-1)][c]=='O'){compte++;}
			if (tab[l][(c-1)]=='O'){compte++;}
			if (tab[(l+1)][(c+1)]=='O'){compte++;}
			if (tab[l][(c+1)]=='O'){compte++;}
			if (tab[(l+1)][c]=='O'){compte++;}
			if (tab[(l-1)][(c+1)]=='O'){compte++;}
			if (tab[(l+1)][(c-1)]=='O'){compte++;}
		return compte;

	}




	char[][] creerplateaualeatoire(int longueur,int hauteur){
		char[][] tab= new char[longueur][hauteur];
		for (int l =0; l < length(tab,1) ; l++ ) {
			for (int c = 0; c < length(tab,2) ; c++ ) {
				if (random() < 0.8) {
					tab[l][c] = '*';	
				}
				else{
					tab[l][c]= 'O';
				}
			}
		}
		return tab;
	}



	void affiche(char[][] tab){
		for (int l =0; l < length(tab,1) ; l++ ) {
			for (int c = 0; c < length(tab,2) ; c++ ) {
				print(tab[l][c]);
			}
			println(" ");
		}
	}
}