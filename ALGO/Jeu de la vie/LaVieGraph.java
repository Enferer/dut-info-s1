import extensions.*;

class JeuDeLaVieGraphique extends Program {

	boolean[][] world;
	Image image;
	int numeroGeneration = 0;

	/*
		Initilisation de tab de manière aléatoire (contrôlé par proba)
		0 <= proba < 1
	*/
	void init(boolean[][] tab, double proba) {
		for (int l=0; l<length(tab,1); l++) {
			for (int c=0; c<length(tab,2); c++) {
				tab[l][c] = (random()<proba);
			}
		}
	}

	/*
		Met à jour d'img suivant le nouvel état de tab
	*/
	void modifieImage(Image img, boolean[][] tab) {
		for (int l=0; l<length(tab,1); l++) {
			for (int c=0; c<length(tab,2); c++) {
				if(tab[l][c]) {
					set(img,l,c, RGBColor.WHITE);
				} else {
					set(img,l,c, RGBColor.BLACK);
				}
			}
		}
	}

	/*
		Affiche le numéro de génération sur l'écran
	*/
	void showGenerationNumber() {
		setColor(image, RGBColor.ORANGE);
		setNewFont(image, "HELVETICA", "BOLD", 52);
		drawString(image,""+numeroGeneration, (int)(getWidth(image)*0.86), (int)(getHeight(image)*0.12));

	}

	/*
		Calcule la prochaine génération à afficher
	*/
	void prochaineGeneration() {
		init(world, 0.3);
		modifieImage(image, world);
		showGenerationNumber();
		numeroGeneration = numeroGeneration + 1;
	}

	/*
		Gestion événements souris
	*/
	void mouseChanged(String name, int x, int y, int button, String event) {
		if (equals(event, "CLICKED")) {
			prochaineGeneration();
		}
	}

	void algorithm() {
		image = newImage(600, 400);
		world = new boolean[getHeight(image)/2][getWidth(image)/2]; 

		setGrid(image, length(world, 1), length(world, 2)); 
		init(world, 0.3);

		modifieImage(image, world);
		showGenerationNumber();

		show(image);

		// nécessaire pour ne pas quitter le programme
		readString();
	}

}