class pendu extends Program{
	
	final String MOT_SECRET = "tunnel";	

	

	void testNewLettre(){
		Lettre l1 = newLettre('a');
		assertEquals('a',l1.lettre);
		assertFalse(l1.decouverte);

	}

	

	Lettre newLettre(char l){
		Lettre res = new Lettre();
		res.lettre = l;
		return res;
	}

	

	String toString(Mot m1){
		String res = "";
		for (int cpt = 0;cpt < length(m1.mot) ; cpt++ ) {
			res += m1.mot[cpt].lettre;
		}
		return res;
	}

	

	String toStringCache(Mot m1){
		String res = "";
		for (int cpt = 0;cpt < length(m1.mot) ; cpt++ ) {
			if (m1.mot[cpt].decouverte) {
				res += m1.mot[cpt].lettre;
			}
			else{res+='*';}
			}
		return res;
	}

	

	Mot newMot(String s) {
		Mot m1 = new Mot();
		m1.mot = new Lettre[length(s)];
		
		for(int cpt = 0; cpt < length(s); cpt++){
			m1.mot[cpt]=newLettre(charAt(s,cpt));
		}
		return m1;	
	}

	
	void algorithm(){
		boolean fini = false;
		Mot m1 = newMot(MOT_SECRET);
		int tentative = 10;

		while(!fini){
			println(toStringCache(m1)+"      | Il te reste "+tentative+" essaies(s)");

			boolean trouve = false;
			char proposition = readChar();
			for (int cpt = 0;cpt < length(MOT_SECRET) ;cpt++) {
				if (m1.mot[cpt].lettre==proposition) {
					m1.mot[cpt].decouverte = true;
					trouve = true;
				}
			}
			if (!trouve) {
				tentative--;
			}
			if (equals(toStringCache(m1),toString(m1))) {
				println("Bien joué tu as trouvé le mot");
				fini = true;
			}
			else if(tentative <= 0){
				println("Tu as perdu");
				fini = true;
			}
		}
	}
}