class Exercice4 extends Program {
    void testNature() {
	assertEquals("ALPHABETIQUE", nature('t'));
	assertEquals("NUMERIQUE", nature('0'));
	assertEquals("AUTRE", nature('$'));
    }
    String nature(char c){
	if((c>=65 && c<=90) || (c>=97 && c<=122)){
	    return("ALPHABETIQUE");
	}
	else if(c>=48 && c<=57 ){
	    return("NUMERIQUE");
	}
	else {
	    return("AUTRE");
	}
    }
}
	
