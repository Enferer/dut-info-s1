class TP4_Exercice1 extends Program {
    void algorithm() {
	println(diviseurs(9));
    }
    String diviseurs(int nbr){
	String resultat = "";
	for(int cpt=1; cpt <= nbr ; cpt++){
	    double x = nbr%cpt;
	    if(x == 0){
		resultat = resultat  + cpt + ",";
	    }
	}
	return resultat;
    }
}
